#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates a CSV file containing the accumulated data of all stipend
applications for easier processing by the stipend team.

Copyright (C) 2020
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys
import json

# retrieve the path to the registration data file on the command line
if(len(sys.argv) < 2):
    data_file = 'registration_data.txt'
    print('Note: no command line argument for a data file was given, therefore this program will choose the file %s' % data_file)
else:
    data_file = sys.argv[1]

# retrieve the path to the output data file on the command line
if(len(sys.argv) < 3):
    out_file = 'stipend_applications.csv'
    print('Note: no command line argument for a output file was given, therefore this program will choose the file %s' % out_file)
else:
    out_file = sys.argv[2]

delimiter = '";"'

csv_rows = []
# try to open the data file
with open(data_file) as f:
    for line in f:
        # check if the current line is JSON data
        if not line.startswith('{'):
            continue
        # if so, parse the json content
        datum = json.loads(line)
        # check if there is a stipend application contained
        if "stipend_application" not in datum:
            continue
        # extract all stipend application relevant data and transform it to CSV
        csv_row = []
        csv_row.append(datum['salutation'])
        csv_row.append(datum['first_name'])
        csv_row.append(datum['last_name'])
        if 'email' not in datum:
            print("Warning! For %s %s the e-Mail entry is missing" % (datum['first_name'], datum['last_name']))
            csv_row.append("")
        else:
            csv_row.append(datum['email'])
        if datum['conference_fee_string'].endswith('Student'):
            csv_row.append('Student')
        elif datum['conference_fee_string'].endswith('Phd'):
            csv_row.append('Phd')
        else:
            csv_row.append('Other')
        csv_row.append(datum['studyfield'])
        if datum['address']['institution'] != "":
            csv_row.append(datum['address']['institution'])
        else:
            csv_row.append(datum['billing_address']['billing_institution'])
        if datum['address']['department'] != "":
            csv_row.append(datum['address']['department'])
        else:
            csv_row.append(datum['billing_address']['billing_department'])
        csv_row.append(datum['stipend_application']['stipend_organisations'])
        csv_row.append(datum['stipend_application']['poster'])
        csv_row.append(datum['stipend_application']['rainbow'])
        csv_row.append(datum['stipend_application']['sunshine_lecturers'])
        csv_row.append(datum['stipend_application']['motivation'])
        csv_row.append(datum['conference_fee_string'])
        csv_row.append('%d' % datum['conference_fee'])
        csv_row.append('%d' % datum['accommodation_fee'])
        csv_row.append('%d' % (datum['conference_fee'] + datum['accommodation_fee']))
        csv_row.append(datum['stipend_application']['monthly_income'])
        csv_row.append(datum['stipend_application']['monthly_expenses'])
        csv_row.append(datum['stipend_application']['additional_funding'])
        csv_row.append(datum['stipend_application']['stipend_requirement'])

        csv_rows.append('"' + delimiter.join(csv_row) + '"')

# write csv data to output file
print('Read %d stipend applications; now attempting to write to %s' % (len(csv_rows), out_file))
with open(out_file, 'w') as f:
    f.write('"Salutation";"First name";"Last name";"E-mail";"Student Status";"Field of Study";"Institution";"Department";"Organisations";"Poster planned";"Rainbow Talk planned";"Sunshine for";"Motivation Letter";"Conference Fee Class";"Conference Fee";"Accommodation Fee";"Total Fee";"Monthly Income";"Monthly Expenses";"Additional Funding";"Urgency"\n')
    f.write('\n'.join(csv_rows))

print('Completed writing')
