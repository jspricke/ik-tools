<script>
/*
* This file defines the schedule data for an IK conference.
* In particular, this file should contain the following data:
* start_date: start date of the conference in the format "YYYY-MM-DD"
* end_date:   end date of the conference in the format "YYYY-MM-DD"
* calendar_url: (optional) A link to an ical file containing schedule data
*               for multiple courses at once, where the course identifier
*               must be the first word in the title of each event.
* courses:      a list of courses, each of which is supposed to be an
*               object with the following attributes:
*   "identifier" : Some unique string identifier for the course, e.g. BC1.
*   "title" :      The string title of the course.
*   "url" :        The URL to the course page.
*   "type" :       The type of the course, either "Basic Course"/"Introductory Course",
*                  "Method Course"/"Advanced Course", "Special Course"/"Focus Course",
*                  "Rainbow Course", "Evening Talk", or "Hack".
*   "instructor" : The name of the instructor(s) as string.
*   "sessions_url" : (optional) a link to an .ics file containing the dates for
*                    the current course. Note that courses may only start on 09:00,
*                    11:00, 14:30, 16:30, or after 18:00.
*   "sessions" :  (optional) A list of session dates, each of which is supposed
*                 to be an object with the following attributes:
*      "date" :   The date of the session in the format "YYYY-MM-DD"
*      "slot" :   The time slot, either "morning" (09:00-10:30),
*                 "noon" (11:00-12:30), "afternoon" (14:30-16:00),
*                 "late-afternoon" (16:30-18:00), or "evening" (after 18:00)
*     "location" : the room number, e.g. "H3", "F1"
*     "start_time" : (optional; only used for "evening" courses)
*                  The exact start time in the format "HH:MM"
*     "end_time" : (optional; only used for "evening" courses)
*                  The exact start time in the format "HH:MM"
*
* Note that the order of courses in the course list also determines the
* order of display. Also note that the course sessions must be defined either
* in the calendar_url, the sessions_url, or the sessions field. If multiple
* fields are defined, the data are merged.
*/
start_date = "2020-03-13";
dinner_date = "2020-03-17";
end_date = "2020-03-20";
calendar_url = "../../schedule/calendar_data_2020.php";
courses = [
/* Block 1: Friday to Sunday (morning and afternoon) */
{
	"identifier" : "IC2",
	"title" : "Introduction to Psychology",
	"url" : "https://interdisciplinary-college.org/2020-ic2/",
	"type" : "Introductory Course",
	"instructor": "Katharina Krämer"
},
{
	"identifier" : "FC09",
	"title" : "Using Robot Models to Explore the Exploratory Behaviour of Insects",
	"short_title" : "Robot Models of Insect Exploratory Behaviour",
	"url" : "https://interdisciplinary-college.org/2020-fc09/",
	"type" : "Focus Course",
	"instructor": "Barbara Webb"
},
{
	"identifier" : "FC10",
	"title" : "Mindfulness as a Method to Explore your Mind-Wandering with Curiosity",
	"short_title" : "Mindfulness and Mind-Wandering",
	"url" : "https://interdisciplinary-college.org/2020-fc10/",
	"type" : "Focus Course",
	"instructor": "Marieke Van Vugt"
},
{
	"identifier" : "FC13",
	"title" : "Hominum-ex-Machina: About Artificial and Real Intelligence",
	"short_title" : "Hominum-ex-Machina",
	"url" : "https://interdisciplinary-college.org/2020-fc13/",
	"type" : "Focus Course",
	"instructor": "Elena Simperl & Markus Krause"
},
{
	"identifier" : "PrfC2",
	"title" : "Ethics in Science and Good Scientific Practice ",
	"url" : "https://interdisciplinary-college.org/2020-prfc2/",
	"type" : "Professional Course",
	"instructor": "Hans-Joachim Pflüger"
},
/* Block 2: Friday to Sunday (noon and late afternoon) */
{
	"identifier" : "IC4",
	"title" : "Introduction to Ethics in AI",
	"url" : "https://interdisciplinary-college.org/2020-ic4/",
	"type" : "Introductory Course",
	"instructor": "Heike Felzmann"
},
{
	"identifier" : "MC1",
	"title" : "Applications of Bayesian Inference and the Free Energy Principle",
	"short_title" : "Bayesian Inference and Free Energy Principle",
	"url" : "https://interdisciplinary-college.org/2020-mc1/",
	"type" : "Method Course",
	"instructor": "Christoph Mathys"
},
{
	"identifier" : "FC07",
	"title" : "A Series of Interesting Choices: Risk, Reward, and Curiosity in Video Games",
	"short_title" : "CRR in Video Games",
	"url" : "https://interdisciplinary-college.org/2020-fc07/",
	"type" : "Focus Course",
	"instructor": "Max Birk"
},
{
	"identifier" : "FC12",
	"title" : "The Development of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc12/",
	"type" : "Focus Course",
	"instructor": "Gert Westermann"
},
{
	"identifier" : "PC2",
	"title" : "Seeking Shaky Ground",
	"url" : "https://interdisciplinary-college.org/2020-pc2/",
	"type" : "Practical Course",
	"instructor": "Claudia Muth & Elisabeth Zimmermann"
},
/* Block 3: Sunday to Tuesday (morning and afternoon) */
{
	"identifier" : "IC3",
	"title" : "Introduction to Neuroscience",
	"url" : "https://interdisciplinary-college.org/2020-ic3/",
	"type" : "Introductory Course",
	"instructor": "Till Bockemühl & Ronald Sladky"
},
{
	"identifier" : "FC03",
	"title" : "Arousal Interactions with Curiosity, Risk, and Reward from a Computational Cognitive Architecture Perspective",
	"short_title" : "Computational Cognitive Architectures of CRR",
	"url" : "https://interdisciplinary-college.org/2020-fc03/",
	"type" : "Focus Course",
	"instructor": "Christopher Dancy"
},
{
	"identifier" : "PC3",
	"title" : "Juggling - experience your brain at work",
	"url" : "https://interdisciplinary-college.org/2020-pc3/",
	"type" : "Practical Course",
	"instructor": "Susan Wache & Julia Wache"
},
{
	"identifier" : "PC4",
	"title" : "Curious Making, Taking Fabrication Risks and Crafting Rewards",
	"short_title" : "Making, Fabrication, and Crafting",
	"url" : "https://interdisciplinary-college.org/2020-pc4/",
	"type" : "Practical Course",
	"instructor": "Janis Meißner"
},
{
	"identifier" : "PrfC1",
	"title" : "Curiosity, Risk, and Reward in Teaching in Higher Education",
	"short_title" : "Teaching in Higher Education",
	"url" : "https://interdisciplinary-college.org/2020-prfc1/",
	"type" : "Professional Course",
	"instructor": "Ingrid Scharlau"
},
{
	"identifier" : "Pan1",
	"title" : "Lived Curiosity in Industry and Academia; panel; title TBA",
	"short_title" : "Industry & Academia Panel Discussion",
	"url" : "https://interdisciplinary-college.org/2020-pan1/",
	"type" : "Additional Event",
	"instructor": "Becky Inkster"
},
/* Block 4: Monday to Tuesday (noon and late afternoon) */
{
	"identifier" : "IC1",
	"title" : "Introduction to Machine Learning",
	"url" : "https://interdisciplinary-college.org/2020-ic1/",
	"type" : "Introductory Course",
	"instructor": "Benjamin Paassen"
},
{
	"identifier" : "FC02",
	"title" : "Your Wit Is My Command",
	"url" : "https://interdisciplinary-college.org/2020-fc02/",
	"type" : "Focus Course",
	"instructor": "Tony Veale"
},
{
	"identifier" : "FC08",
	"title" : "The Motivational Power of Curiosity – Information as Reward",
	"short_title" : "The Motivational Power of Curiosity",
	"url" : "https://interdisciplinary-college.org/2020-fc08/",
	"type" : "Focus Course",
	"instructor": "Lily FitzGibbon"
},
{
	"identifier" : "Db1",
	"title" : "Curiosity as a research field; debate; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-db1/",
	"type" : "Additional Event",
	"instructor": "Praveen Paritosh"
},
/* Block 5: Tuesday to Thursday (morning and afternoon) */
{
	"identifier" : "MC2",
	"title" : "Symbolic Reasoning within Connectionist Systems",
	"url" : "https://interdisciplinary-college.org/2020-mc2/",
	"type" : "Method Course",
	"instructor": "Klaus Greff"
},
{
	"identifier" : "FC05",
	"title" : "Confidence and Overconfidence",
	"url" : "https://interdisciplinary-college.org/2020-fc05/",
	"type" : "Focus Course",
	"instructor": "Vivek Nityananda"
},
{
	"identifier" : "FC06",
	"title" : "Cybersecurity/Curiosity, Risk, and Reward in Hacking; title TBA",
	"short_title" : "Cybersecurity",
	"url" : "https://interdisciplinary-college.org/2020-fc06/",
	"type" : "Focus Course",
	"instructor": "Sadia Afroz"
},
{
	"identifier" : "FC11",
	"title" : "Artificial curiosity for robot learning",
	"url" : "https://interdisciplinary-college.org/2020-fc11/",
	"type" : "Focus Course",
	"instructor": "Nguyen Sao Mai"
},
{
	"identifier" : "PC1",
	"title" : "Perceiving the World through Introspection",
	"url" : "https://interdisciplinary-college.org/2020-pc1/",
	"type" : "Practical Course",
	"instructor": "Annekatrin Vetter & Sophia Reul"
},
/* Block 6: Tuesday to Friday (noon and late afternoon) */
{
	"identifier" : "PrfC3",
	"title" : "Curiosity, Risk, and Reward in the Academic Job Search",
	"short_title" : "Academic Job Search",
	"url" : "https://interdisciplinary-college.org/2020-prfc3/",
	"type" : "Professional Course",
	"instructor": "Emily King"
},
{
	"identifier" : "MC3",
	"title" : "Embodied Symbol Emergence",
	"url" : "https://interdisciplinary-college.org/2020-mc3/",
	"type" : "Method Course",
	"instructor": "Malte Schilling & Michael Spranger"
},
{
	"identifier" : "MC4",
	"title" : "Learning Mappings via Symbolic, Probabilistic, and Connectionist Modeling",
	"short_title" : "Learning Mappings",
	"url" : "https://interdisciplinary-college.org/2020-mc4/",
	"type" : "Method Course",
	"instructor": "Afsaneh Fazly"
},
{
	"identifier" : "MC5",
	"title" : "Low Complexity Modeling in Data Analysis and Image Processing",
	"short_title" : "Low Complexity Modeling",
	"url" : "https://interdisciplinary-college.org/2020-mc5/",
	"type" : "Method Course",
	"instructor": "Emily King"
},
{
	"identifier" : "FC01",
	"title" : "Motifs for Neurocognitive Challenges from Individual to Evolutionary Time Scales",
	"short_title" : "Neurocognitive Challenges",
	"url" : "https://interdisciplinary-college.org/2020-fc01/",
	"type" : "Focus Course",
	"instructor": "Wulf Haubensak"
},
{
	"identifier" : "FC04",
	"title" : "Behaviour Modelling; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-fc04/",
	"type" : "Focus Course",
	"instructor": "Fahim Kawsar"
},
/* Evening and Additional Events */
{
	"identifier" : "",
	"title" : "Welcome Address",
	"url" : "https://interdisciplinary-college.org/program/#events",
	"type" : "Evening Talk",
	"instructor": "Smeddinck, Rohlfing & Stewart",
	"sessions" : [
		{ "date" : "2020-03-13", "slot" : "evening", "start_time" : "20:00", "end_time" : "21:30", "location" : "Hall" }
	]
},
{
	"identifier" : "ET1",
	"title" : "How to Know",
	"url" : "https://interdisciplinary-college.org/2020-et1/",
	"type" : "Evening Talk",
	"instructor": "Celeste Kidd"
},
{
	"identifier" : "",
	"title" : "Poster Session",
	"url" : "https://interdisciplinary-college.org/contribute/#poster",
	"type" : "Evening Talk",
	"instructor": "",
	"sessions" : [
		{ "date" : "2020-03-15", "slot" : "evening", "start_time" : "20:00", "end_time" : "22:30", "location" : "Forum" }
	]
},
{
	"identifier" : "ET2",
	"title" : "Data-Driven Dynamical Models for Neuroscience and Neuroengineering",
	"url" : "https://interdisciplinary-college.org/2020-et2/",
	"type" : "Evening Talk",
	"instructor": "Bing W. Brunton"
},
{
	"identifier" : "ET3",
	"title" : "Information as a Resource: How Organisms Deal with Uncertainty",
	"url" : "https://interdisciplinary-college.org/2020-et3/",
	"type" : "Evening Talk",
	"instructor": "Alex Kacelnik"
},
{
	"identifier" : "ET4",
	"title" : "Cognitive Systems; title TBA",
	"url" : "https://interdisciplinary-college.org/2020-et4/",
	"type" : "Evening Talk",
	"instructor": "Tanja Schultz"
}
];
</script>
