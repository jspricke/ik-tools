/*
* This file defines the schedule data for an IK conference.
* In particular, this file should contain the following data:
* start_date: start date of the conference in the format "YYYY-MM-DD"
* end_date:   end date of the conference in the format "YYYY-MM-DD"
* calendar_url: (optional) A link to an ical file containing schedule data
*               for multiple courses at once, where the course identifier
*               must be the first word in the title of each event.
* courses:      a list of courses, each of which is supposed to be an
*               object with the following attributes:
*   "identifier" : Some unique string identifier for the course, e.g. BC1.
*   "title" :      The string title of the course.
*   "url" :        The URL to the course page.
*   "type" :       The type of the course, either "Basic Course"/"Introductory Course",
*                  "Method Course"/"Advanced Course", "Special Course"/"Focus Course",
*                  "Rainbow Course", "Evening Talk", or "Hack".
*   "instructor" : The name of the instructor(s) as string.
*   "sessions_url" : (optional) a link to an .ics file containing the dates for
*                    the current course. Note that courses may only start on 09:00,
*                    11:00, 14:30, 16:30, or after 18:00.
*   "sessions" :  (optional) A list of session dates, each of which is supposed
*                 to be an object with the following attributes:
*      "date" :   The date of the session in the format "YYYY-MM-DD"
*      "slot" :   The time slot, either "morning" (09:00-10:30),
*                 "noon" (11:00-12:30), "afternoon" (14:30-16:00),
*                 "late-afternoon" (16:30-18:00), or "evening" (after 18:00)
*     "location" : the room number, e.g. "H3", "F1"
*     "start_time" : (optional; only used for "evening" courses)
*                  The exact start time in the format "HH:MM"
*     "end_time" : (optional; only used for "evening" courses)
*                  The exact start time in the format "HH:MM"
*
* Note that the order of courses in the course list also determines the
* order of display. Also note that the course sessions must be defined either
* in the calendar_url, the sessions_url, or the sessions field. If multiple
* fields are defined, the data are merged.
*/
start_date = "2020-03-13";
dinner_date = "2020-03-17";
end_date = "2020-03-20";
courses = [
/* Block 1: Friday to Sunday (morning and afternoon) */
{
    "identifier" : "IC2",
    "title" : "Introduction to Psychology",
    "url" : "https://interdisciplinary-college.org/2020-ic2/",
    "type" : "Introductory Course",
    "instructor": "Katharina Krämer"
},
{
    "identifier" : "FC09",
    "title" : "Using Robot Models to Explore the Exploratory Behaviour of Insects",
    "short_title" : "Robot Models of Insect Exploratory Behaviour",
    "url" : "https://interdisciplinary-college.org/2020-fc09/",
    "type" : "Focus Course",
    "instructor": "Barbara Webb"
},
{
    "identifier" : "FC10",
    "title" : "Mindfulness as a Method to Explore your Mind-Wandering with Curiosity",
    "short_title" : "Mindfulness and Mind-Wandering",
    "url" : "https://interdisciplinary-college.org/2020-fc10/",
    "type" : "Focus Course",
    "instructor": "Marieke Van Vugt"
},
{
    "identifier" : "FC13",
    "title" : "Hominum-ex-Machina: About Artificial and Real Intelligence",
    "short_title" : "Hominum-ex-Machina",
    "url" : "https://interdisciplinary-college.org/2020-fc13/",
    "type" : "Focus Course",
    "instructor": "Elena Simperl & Markus Krause"
},
{
    "identifier" : "PrfC2",
    "title" : "Ethics in Science and Good Scientific Practice ",
    "url" : "https://interdisciplinary-college.org/2020-prfc2/",
    "type" : "Professional Course",
    "instructor": "Hans-Joachim Pflüger"
},
/* Block 2: Friday to Sunday (noon and late afternoon) */
{
    "identifier" : "IC4",
    "title" : "Introduction to Ethics in AI",
    "url" : "https://interdisciplinary-college.org/2020-ic4/",
    "type" : "Introductory Course",
    "instructor": "Heike Felzmann"
},
{
    "identifier" : "MC1",
    "title" : "Applications of Bayesian Inference and the Free Energy Principle",
    "short_title" : "Bayesian Inference and Free Energy Principle",
    "url" : "https://interdisciplinary-college.org/2020-mc1/",
    "type" : "Method Course",
    "instructor": "Christoph Mathys"
},
{
    "identifier" : "FC07",
    "title" : "A Series of Interesting Choices: Risk, Reward, and Curiosity in Video Games",
    "short_title" : "CRR in Video Games",
    "url" : "https://interdisciplinary-college.org/2020-fc07/",
    "type" : "Focus Course",
    "instructor": "Max Birk"
},
{
    "identifier" : "FC12",
    "title" : "The Development of Curiosity",
    "url" : "https://interdisciplinary-college.org/2020-fc12/",
    "type" : "Focus Course",
    "instructor": "Gert Westermann"
},
{
    "identifier" : "PC2",
    "title" : "Seeking Shaky Ground",
    "url" : "https://interdisciplinary-college.org/2020-pc2/",
    "type" : "Practical Course",
    "instructor": "Claudia Muth & Elisabeth Zimmermann"
},
/* Block 3: Sunday to Tuesday (morning and afternoon) */
{
    "identifier" : "IC3",
    "title" : "Introduction to Neuroscience",
    "url" : "https://interdisciplinary-college.org/2020-ic3/",
    "type" : "Introductory Course",
    "instructor": "Till Bockemühl & Ronald Sladky"
},
{
    "identifier" : "FC03",
    "title" : "Arousal Interactions with Curiosity, Risk, and Reward from a Computational Cognitive Architecture Perspective",
    "short_title" : "Computational Cognitive Architectures of CRR",
    "url" : "https://interdisciplinary-college.org/2020-fc03/",
    "type" : "Focus Course",
    "instructor": "Christopher Dancy"
},
{
    "identifier" : "PC3",
    "title" : "Juggling - experience your brain at work",
    "url" : "https://interdisciplinary-college.org/2020-pc3/",
    "type" : "Practical Course",
    "instructor": "Susan Wache & Julia Wache"
},
{
    "identifier" : "PC4",
    "title" : "Curious Making, Taking Fabrication Risks and Crafting Rewards",
    "short_title" : "Making, Fabrication, and Crafting",
    "url" : "https://interdisciplinary-college.org/2020-pc4/",
    "type" : "Practical Course",
    "instructor": "Janis Meißner"
},
{
    "identifier" : "PrfC1",
    "title" : "Curiosity, Risk, and Reward in Teaching in Higher Education",
    "short_title" : "Teaching in Higher Education",
    "url" : "https://interdisciplinary-college.org/2020-prfc1/",
    "type" : "Professional Course",
    "instructor": "Ingrid Scharlau"
},
{
    "identifier" : "Pan1",
    "title" : "Lived Curiosity in Industry and Academia; panel; title TBA",
    "short_title" : "Industry & Academia Panel Discussion",
    "url" : "https://interdisciplinary-college.org/2020-pan1/",
    "type" : "Additional Event",
    "instructor": "Becky Inkster"
},
/* Block 4: Monday to Tuesday (noon and late afternoon) */
{
    "identifier" : "IC1",
    "title" : "Introduction to Machine Learning",
    "url" : "https://interdisciplinary-college.org/2020-ic1/",
    "type" : "Introductory Course",
    "instructor": "Benjamin Paassen"
},
{
    "identifier" : "FC02",
    "title" : "Your Wit Is My Command",
    "url" : "https://interdisciplinary-college.org/2020-fc02/",
    "type" : "Focus Course",
    "instructor": "Tony Veale"
},
{
    "identifier" : "FC08",
    "title" : "The Motivational Power of Curiosity – Information as Reward",
    "short_title" : "The Motivational Power of Curiosity",
    "url" : "https://interdisciplinary-college.org/2020-fc08/",
    "type" : "Focus Course",
    "instructor": "Lily FitzGibbon"
},
{
    "identifier" : "Db1",
    "title" : "Curiosity as a research field; debate; title TBA",
    "url" : "https://interdisciplinary-college.org/2020-db1/",
    "type" : "Additional Event",
    "instructor": "Praveen Paritosh"
},
/* Block 5: Tuesday to Thursday (morning and afternoon) */
{
    "identifier" : "MC2",
    "title" : "Symbolic Reasoning within Connectionist Systems",
    "url" : "https://interdisciplinary-college.org/2020-mc2/",
    "type" : "Method Course",
    "instructor": "Klaus Greff"
},
{
    "identifier" : "FC05",
    "title" : "Confidence and Overconfidence",
    "url" : "https://interdisciplinary-college.org/2020-fc05/",
    "type" : "Focus Course",
    "instructor": "Vivek Nityananda"
},
{
    "identifier" : "FC06",
    "title" : "Cybersecurity/Curiosity, Risk, and Reward in Hacking; title TBA",
    "short_title" : "Cybersecurity",
    "url" : "https://interdisciplinary-college.org/2020-fc06/",
    "type" : "Focus Course",
    "instructor": "Sadia Afroz"
},
{
    "identifier" : "FC11",
    "title" : "Artificial curiosity for robot learning",
    "url" : "https://interdisciplinary-college.org/2020-fc11/",
    "type" : "Focus Course",
    "instructor": "Nguyen Sao Mai"
},
{
    "identifier" : "PC1",
    "title" : "Perceiving the World through Introspection",
    "url" : "https://interdisciplinary-college.org/2020-pc1/",
    "type" : "Practical Course",
    "instructor": "Annekatrin Vetter & Sophia Reul"
},
/* Block 6: Tuesday to Friday (noon and late afternoon) */
{
    "identifier" : "PrfC3",
    "title" : "Curiosity, Risk, and Reward in the Academic Job Search",
    "short_title" : "Academic Job Search",
    "url" : "https://interdisciplinary-college.org/2020-prfc3/",
    "type" : "Professional Course",
    "instructor": "Emily King"
},
{
    "identifier" : "MC3",
    "title" : "Embodied Symbol Emergence",
    "url" : "https://interdisciplinary-college.org/2020-mc3/",
    "type" : "Method Course",
    "instructor": "Malte Schilling & Michael Spranger"
},
{
    "identifier" : "MC4",
    "title" : "Learning Mappings via Symbolic, Probabilistic, and Connectionist Modeling",
    "short_title" : "Learning Mappings",
    "url" : "https://interdisciplinary-college.org/2020-mc4/",
    "type" : "Method Course",
    "instructor": "Afsaneh Fazly"
},
{
    "identifier" : "MC5",
    "title" : "Low Complexity Modeling in Data Analysis and Image Processing",
    "short_title" : "Low Complexity Modeling",
    "url" : "https://interdisciplinary-college.org/2020-mc5/",
    "type" : "Method Course",
    "instructor": "Emily King"
},
{
    "identifier" : "FC01",
    "title" : "Motifs for Neurocognitive Challenges from Individual to Evolutionary Time Scales",
    "short_title" : "Neurocognitive Challenges",
    "url" : "https://interdisciplinary-college.org/2020-fc01/",
    "type" : "Focus Course",
    "instructor": "Wulf Haubensak"
},
{
    "identifier" : "FC04",
    "title" : "Behaviour Modelling; title TBA",
    "url" : "https://interdisciplinary-college.org/2020-fc04/",
    "type" : "Focus Course",
    "instructor": "Fahim Kawsar"
},
/* Evening and Additional Events */
{
    "identifier" : "",
    "title" : "Welcome Address",
    "url" : "https://interdisciplinary-college.org/program/#events",
    "type" : "Evening Talk",
    "instructor": "Smeddinck, Rohlfing & Stewart",
    "sessions" : [
        { "date" : "2020-03-13", "slot" : "evening", "start_time" : "20:00", "end_time" : "21:30", "location" : "Hall" }
    ]
},
{
    "identifier" : "ET1",
    "title" : "How to Know",
    "url" : "https://interdisciplinary-college.org/2020-et1/",
    "type" : "Evening Talk",
    "instructor": "Celeste Kidd"
},
{
    "identifier" : "",
    "title" : "Poster Session",
    "url" : "https://interdisciplinary-college.org/contribute/#poster",
    "type" : "Evening Talk",
    "instructor": "",
    "sessions" : [
        { "date" : "2020-03-15", "slot" : "evening", "start_time" : "20:00", "end_time" : "22:30", "location" : "Forum" }
    ]
},
{
    "identifier" : "ET2",
    "title" : "Data-Driven Dynamical Models for Neuroscience and Neuroengineering",
    "url" : "https://interdisciplinary-college.org/2020-et2/",
    "type" : "Evening Talk",
    "instructor": "Bing W. Brunton"
},
{
    "identifier" : "ET3",
    "title" : "Information as a Resource: How Organisms Deal with Uncertainty",
    "url" : "https://interdisciplinary-college.org/2020-et3/",
    "type" : "Evening Talk",
    "instructor": "Alex Kacelnik"
},
{
    "identifier" : "ET4",
    "title" : "Cognitive Systems; title TBA",
    "url" : "https://interdisciplinary-college.org/2020-et4/",
    "type" : "Evening Talk",
    "instructor": "Tanja Schultz"
}
];
calendar_data = `BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:IK 2020
X-WR-TIMEZONE:UTC
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:186mnqgahg7ui1up6nktek3et1@google.com
CREATED:20191227T135711Z
DESCRIPTION:
LAST-MODIFIED:20200117T102309Z
LOCATION:H4
SEQUENCE:6
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T163000Z
DTEND:20200315T180000Z
DTSTAMP:20200204T230237Z
UID:77p5a6sooodjgg1trqonmosngn@google.com
CREATED:20191227T123613Z
DESCRIPTION:
LAST-MODIFIED:20200117T102300Z
LOCATION:H4
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:48qhqpaon9kb2dffqq80coc2ja@google.com
CREATED:20191227T120138Z
DESCRIPTION:
LAST-MODIFIED:20200116T103622Z
LOCATION:H3
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:7muai2i09182rgpfr7iq77iiv9@google.com
CREATED:20191227T120508Z
DESCRIPTION:
LAST-MODIFIED:20200116T103531Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:01aeog5p1k8v52v5rk3lsoh9ih@google.com
CREATED:20191227T123619Z
DESCRIPTION:
LAST-MODIFIED:20200116T103506Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:14qmst7dpjo2lm457krc7dhndj@google.com
CREATED:20191227T121320Z
DESCRIPTION:
LAST-MODIFIED:20200116T103457Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:7tsard6ogjk4j43ipioagi8ctl@google.com
CREATED:20191227T121141Z
DESCRIPTION:
LAST-MODIFIED:20200116T103443Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:1dmceia3gq57s7s4dv7c6tipeb@google.com
CREATED:20191227T121313Z
DESCRIPTION:
LAST-MODIFIED:20200116T103438Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:5idncchra324609q03htcru7gn@google.com
CREATED:20191227T121328Z
DESCRIPTION:
LAST-MODIFIED:20200116T103431Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC12: The Development of Curiosity (Westermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:5c8ph9obqn1lek6e137nq4vej1@google.com
CREATED:20191227T121235Z
DESCRIPTION:
LAST-MODIFIED:20200116T103427Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:1pt58f0ljq8702kvndlf8mbi8v@google.com
CREATED:20191227T120426Z
DESCRIPTION:
LAST-MODIFIED:20200116T103410Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:58621aeaac3bk8cegnvaqhhdgb@google.com
CREATED:20191227T121241Z
DESCRIPTION:
LAST-MODIFIED:20200116T103406Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:095kh7rmgh1a59ivvt5i3of1u9@google.com
CREATED:20191227T123547Z
DESCRIPTION:
LAST-MODIFIED:20200116T103351Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:4fgfrqmbcrmeotu3pi48scja79@google.com
CREATED:20191227T120514Z
DESCRIPTION:
LAST-MODIFIED:20200116T103310Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:3otqa9l60k1ehf72l3rm254o0o@google.com
CREATED:20191227T121148Z
DESCRIPTION:
LAST-MODIFIED:20200116T103306Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC10: Mindfulness as a Method to Explore your Mind-Wandering with C
 uriosity (Van Vugt)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:3nelli45keqcbpumvscc5ojfj6@google.com
CREATED:20191227T120400Z
DESCRIPTION:
LAST-MODIFIED:20200116T103147Z
LOCATION:H3
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PrfC2: Ethics in Science and Good Scientific Practice (Pflüger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:0dpp05da6vu5v4ddtq0u1qdt9t@google.com
CREATED:20191227T123532Z
DESCRIPTION:
LAST-MODIFIED:20200116T103141Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PC2: Seeking Shaky Ground (Muth &amp; Zimmermann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:2fjv0q40oj0qj61cv6g330fvpa@google.com
CREATED:20191227T123326Z
DESCRIPTION:
LAST-MODIFIED:20200116T101305Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:4h5cid91lefo3sjfngoqmphek5@google.com
CREATED:20191227T123244Z
DESCRIPTION:
LAST-MODIFIED:20200116T101258Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:2vi84lqof9r0gj6p0970sf3b9h@google.com
CREATED:20191227T123202Z
DESCRIPTION:
LAST-MODIFIED:20200116T101248Z
LOCATION:H2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:1mssnlldu3nunl6k26mq24vnks@google.com
CREATED:20191227T123333Z
DESCRIPTION:
LAST-MODIFIED:20200116T101242Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC3:  AI and Linguistics (Schilling &amp; Spranger)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:6si7cuht1lno1p6l4d5f7dq4mp@google.com
CREATED:20191227T135703Z
DESCRIPTION:
LAST-MODIFIED:20200116T101143Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:25t1f3i4i7eodt7vhmlj7kea70@google.com
CREATED:20191227T135650Z
DESCRIPTION:
LAST-MODIFIED:20200116T101136Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:70qu1vkfcbqes0qufqb5rcdb43@google.com
CREATED:20191227T135633Z
DESCRIPTION:
LAST-MODIFIED:20200116T101129Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC07: Risk\, Reward\, and Curiosity in Video Games (Birk)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:25r8ltimv0ptbjn8lnac2g19sf@google.com
CREATED:20191227T103533Z
DESCRIPTION:
LAST-MODIFIED:20200116T101113Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:0tpe9r0r11l99ej6b3d5p4s9al@google.com
CREATED:20191227T103605Z
DESCRIPTION:
LAST-MODIFIED:20200116T101109Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:39u8ubu2ids6og6jk73vu1ujss@google.com
CREATED:20191227T103613Z
DESCRIPTION:
LAST-MODIFIED:20200116T101103Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:0ulori0vkgglq9d8kg5bk702tt@google.com
CREATED:20191227T103548Z
DESCRIPTION:
LAST-MODIFIED:20200116T101058Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC3: Introduction to Neuroscience (Bockemühl &amp; Sladky)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:0dthk2h0k7ggpocm691amomdrt@google.com
CREATED:20191227T122743Z
DESCRIPTION:
LAST-MODIFIED:20200116T032922Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:2md2964b2b5apfratmn9u0uv3l@google.com
CREATED:20191227T122730Z
DESCRIPTION:
LAST-MODIFIED:20200116T032917Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:4d0qp6ekepgjjmp10er8oihgab@google.com
CREATED:20191227T133649Z
DESCRIPTION:
LAST-MODIFIED:20200116T032911Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:5v73v4li552bo8e2tnu7gtagea@google.com
CREATED:20191227T135740Z
DESCRIPTION:
LAST-MODIFIED:20200116T032904Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:0g9ji5cgc4iig0qkvpdbgo1mk0@google.com
CREATED:20191227T135730Z
DESCRIPTION:
LAST-MODIFIED:20200116T032857Z
LOCATION:H2
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:53j7lcqdni8grnra8pjlrg953f@google.com
CREATED:20191227T135754Z
DESCRIPTION:
LAST-MODIFIED:20200116T032853Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC04: Behaviour Modelling (Kawsar)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:157mgalmdiocsurqd701glsm5u@google.com
CREATED:20191227T133536Z
DESCRIPTION:
LAST-MODIFIED:20200116T032817Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200320T090000Z
DTEND:20200320T103000Z
DTSTAMP:20200204T230237Z
UID:43j65kct1n8d67dmk1ug97e77o@google.com
CREATED:20191227T133541Z
DESCRIPTION:
LAST-MODIFIED:20200116T032814Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:52bdm8vluetvf8hrh54s8qbkr9@google.com
CREATED:20191227T124435Z
DESCRIPTION:
LAST-MODIFIED:20200116T032803Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC5: Low Complexity Modeling in Data Analysis and Image Processing 
 (King)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:72mjlv6hg3pcoq2iblt3odsfua@google.com
CREATED:20191227T124424Z
DESCRIPTION:
LAST-MODIFIED:20200116T032758Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PrfC3: Curiosity\, Risk\, and Reward in the Academic Job Search (Ki
 ng)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:7k9oq7sbrp0p94vddjs4b83k1s@google.com
CREATED:20191227T122722Z
DESCRIPTION:
LAST-MODIFIED:20200116T032737Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC01: Motifs for Neurocognitive Challenges from Individual to Evolu
 tionary Time Scales (Haubensack)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:0i779g606hl4cj5u10id3dgepm@google.com
CREATED:20200107T190609Z
DESCRIPTION:
LAST-MODIFIED:20200116T032637Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:17l8h9gljrclim87ok8srl401f@google.com
CREATED:20200107T190555Z
DESCRIPTION:
LAST-MODIFIED:20200116T032627Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:16kra8qti4l765n2phhqgl9r45@google.com
CREATED:20191227T151007Z
DESCRIPTION:
LAST-MODIFIED:20200116T032620Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:59e3ta4o8pdukdilipamka954q@google.com
CREATED:20200107T190632Z
DESCRIPTION:
LAST-MODIFIED:20200116T032616Z
LOCATION:F2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Db1: Curiosity as a research field\; debate (Paritosh)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:619vq6ib346lq2pmqcvoj4umn0@google.com
CREATED:20191227T114146Z
DESCRIPTION:
LAST-MODIFIED:20200116T032557Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:7ni4n4pk7sps2gkkplmsmav207@google.com
CREATED:20191227T123448Z
DESCRIPTION:
LAST-MODIFIED:20200116T032553Z
LOCATION:F3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:69c1lgo69balpagt4heav18n4g@google.com
CREATED:20191227T123906Z
DESCRIPTION:
LAST-MODIFIED:20200116T032510Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:29523prlobc6mr6tmaubp97s46@google.com
CREATED:20191227T124134Z
DESCRIPTION:
LAST-MODIFIED:20200116T032506Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:66es4ekb3n8vjuanvfvkilfep7@google.com
CREATED:20191227T114026Z
DESCRIPTION:
LAST-MODIFIED:20200116T032437Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:3t5o7ucanf0f4bi62h82foddmo@google.com
CREATED:20191227T123438Z
DESCRIPTION:
LAST-MODIFIED:20200116T032432Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:520qh2e246mabtij9r9ksh7fqa@google.com
CREATED:20191227T123726Z
DESCRIPTION:
LAST-MODIFIED:20200116T032416Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:3b271l5kcc6qckktq9ubbeluc9@google.com
CREATED:20191227T115633Z
DESCRIPTION:
LAST-MODIFIED:20200116T032402Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:1cfqjatbg5s5lja5oguth417k5@google.com
CREATED:20191227T115609Z
DESCRIPTION:
LAST-MODIFIED:20200116T032354Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:3hedso6callekbsbg48indkdlv@google.com
CREATED:20191227T120200Z
DESCRIPTION:
LAST-MODIFIED:20200116T032345Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:5aa0h6c5dbeeahij1h0vkri30t@google.com
CREATED:20191227T123731Z
DESCRIPTION:
LAST-MODIFIED:20200116T032340Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:25sajnfmjeeucbk80m7mko48k2@google.com
CREATED:20191227T123717Z
DESCRIPTION:
LAST-MODIFIED:20200116T032335Z
LOCATION:H2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:4en2ag4ntpgpf850adjmrdgkl5@google.com
CREATED:20191227T120154Z
DESCRIPTION:
LAST-MODIFIED:20200116T032328Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:6snjsgsraahkoes3k8cdkqlei9@google.com
CREATED:20191227T115539Z
DESCRIPTION:
LAST-MODIFIED:20200116T032323Z
LOCATION:H3
SEQUENCE:7
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:3l8ilpt3ri81ra265ab7bvq21a@google.com
CREATED:20191227T114345Z
DESCRIPTION:
LAST-MODIFIED:20200116T030639Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:64gh0f652p4nooru0hhkgllctu@google.com
CREATED:20191227T123455Z
DESCRIPTION:
LAST-MODIFIED:20200116T030631Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:5272vqtecim9u0vrdgk3ll9nfn@google.com
CREATED:20191227T114049Z
DESCRIPTION:
LAST-MODIFIED:20200116T030621Z
LOCATION:H4
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC1: Introduction to Machine Learning (Paaßen)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:3drg0cd5g4kkg2li1rr3s0h5o1@google.com
CREATED:20191227T123431Z
DESCRIPTION:
LAST-MODIFIED:20200116T030559Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC08: The Motivational Power of Curiosity – Information as Reward (
 Fitzgibbon)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:6nsf9qtn87mssamg017dapqd2e@google.com
CREATED:20191227T120146Z
DESCRIPTION:
LAST-MODIFIED:20200116T030437Z
LOCATION:H4
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:PC3: Juggling - experience your brain at work (Wache &amp; Wache)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:35hj4s4962cqf9s4a1g3n5ihfl@google.com
CREATED:20191227T123709Z
DESCRIPTION:
LAST-MODIFIED:20200116T030432Z
LOCATION:H2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PrfC1: Curiosity\, Risk\, and Reward in Teaching in Higher Educatio
 n (Scharlau)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:2vpgbs3or4fru3ohh0jrmef5kn@google.com
CREATED:20191227T115621Z
DESCRIPTION:
LAST-MODIFIED:20200116T030319Z
LOCATION:H3
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC03: Arousal Interactions with Curiosity\, Risk\, and Reward (Danc
 y)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:5mbb1dms92nrbhtumtooaevp0j@google.com
CREATED:20191227T103327Z
DESCRIPTION:
LAST-MODIFIED:20200116T025740Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:48asld3utm78mih95kdqhc4060@google.com
CREATED:20191227T121856Z
DESCRIPTION:
LAST-MODIFIED:20200116T025547Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:64dv40av1bu9fo96gd1loia2h1@google.com
CREATED:20191227T121834Z
DESCRIPTION:
LAST-MODIFIED:20200116T025536Z
LOCATION:F1
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:40tjted2h15cg5mv8evhl9cppe@google.com
CREATED:20191227T121849Z
DESCRIPTION:
LAST-MODIFIED:20200116T025531Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:5orejl6jk06hdf3be15h54ido6@google.com
CREATED:20191227T121825Z
DESCRIPTION:
LAST-MODIFIED:20200116T025525Z
LOCATION:F1
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC1:  Applications of Bayesian Inference and the Free Energy Princi
 ple (Mathys)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:3s2rmfs64dr8ou24po4mjkmh02@google.com
CREATED:20191227T103318Z
DESCRIPTION:
LAST-MODIFIED:20200116T025404Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:79b1smm9umnqce8c786lvhsrdk@google.com
CREATED:20191227T103305Z
DESCRIPTION:
LAST-MODIFIED:20200116T025347Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:4j15phjk6vm96ibr9pic58k12s@google.com
CREATED:20191227T103159Z
DESCRIPTION:
LAST-MODIFIED:20200116T025337Z
LOCATION:H2
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:IC2: Introduction to Psychology (Krämer)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:17t9p9anjhe9btmfi5lv2g01n0@google.com
CREATED:20191227T120614Z
DESCRIPTION:
LAST-MODIFIED:20200116T025212Z
LOCATION:F1
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:0d7d5kntl2p67jrf63h0ncs3bm@google.com
CREATED:20191227T120620Z
DESCRIPTION:
LAST-MODIFIED:20200116T025200Z
LOCATION:F1
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:7j670ou75d6edatemml9qrppkr@google.com
CREATED:20191227T120551Z
DESCRIPTION:
LAST-MODIFIED:20200116T025154Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:0siussur61qmgklv06cpbujscg@google.com
CREATED:20191227T120603Z
DESCRIPTION:
LAST-MODIFIED:20200116T025147Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC09: Using Robot Models to Explore the Exploratory Behaviour of In
 sects (Webb)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T163000Z
DTEND:20200318T180000Z
DTSTAMP:20200204T230237Z
UID:7m7vdecaboop00veq0m7sgb5k5@google.com
CREATED:20191227T115101Z
DESCRIPTION:
LAST-MODIFIED:20200116T012639Z
LOCATION:H4
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T163000Z
DTEND:20200319T180000Z
DTSTAMP:20200204T230237Z
UID:0vrafdu4rh381odci2cpktjeif@google.com
CREATED:20191227T115028Z
DESCRIPTION:
LAST-MODIFIED:20200116T012631Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T110000Z
DTEND:20200319T123000Z
DTSTAMP:20200204T230237Z
UID:6l8cqqjej0lt16ele03238or9q@google.com
CREATED:20191227T115036Z
DESCRIPTION:
LAST-MODIFIED:20200116T012627Z
LOCATION:H4
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:MC4: Learning Mappings via Symbolic\, Probabilistic\, and Connectio
 nist Modeling (Fazly)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:77sagh9sdnk6m0vf6k0k95a8lv@google.com
CREATED:20191227T124034Z
DESCRIPTION:
LAST-MODIFIED:20200116T005913Z
LOCATION:H2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:6gqe1i6qria1q0d7esb97d5l8m@google.com
CREATED:20191227T124058Z
DESCRIPTION:
LAST-MODIFIED:20200116T005903Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:4cdfi9d32sopctdimmfhmdve2b@google.com
CREATED:20191227T124106Z
DESCRIPTION:
LAST-MODIFIED:20200116T005842Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:2c42rlhkbhkp8s93cu7cu0plu0@google.com
CREATED:20191227T124046Z
DESCRIPTION:
LAST-MODIFIED:20200116T005833Z
LOCATION:H2
SEQUENCE:4
STATUS:CONFIRMED
SUMMARY:FC11: Artificial curiosity for robot learning (Mai)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T143000Z
DTEND:20200314T160000Z
DTSTAMP:20200204T230237Z
UID:1etr9djtd40amje6fd0nieh7b8@google.com
CREATED:20200115T231059Z
DESCRIPTION:
LAST-MODIFIED:20200115T231101Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:31apcqakpt556ha64e21q4h3mo@google.com
CREATED:20191227T123036Z
DESCRIPTION:
LAST-MODIFIED:20200103T134800Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:72isal8l7n04r1mqsnrg2vf95j@google.com
CREATED:20191227T123020Z
DESCRIPTION:
LAST-MODIFIED:20200103T134756Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:76g7utamcgukq1omouhdiefaf5@google.com
CREATED:20191227T123028Z
DESCRIPTION:
LAST-MODIFIED:20200103T134751Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:441cfbq0dhqmonaihf3m8jop6g@google.com
CREATED:20191227T123011Z
DESCRIPTION:
LAST-MODIFIED:20200103T134745Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC06:  Cybersecurity/Curiosity\, Risk\, and Reward in Hacking (Afro
 z)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T143000Z
DTEND:20200315T160000Z
DTSTAMP:20200204T230237Z
UID:0no8tdqnvljbc7jvgad2ldbmge@google.com
CREATED:20191227T120254Z
DESCRIPTION:
LAST-MODIFIED:20200103T134639Z
LOCATION:F2
SEQUENCE:5
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T163000Z
DTEND:20200317T180000Z
DTSTAMP:20200204T230237Z
UID:7pg7autvefut9le8r3lea587v5@google.com
CREATED:20191227T122823Z
DESCRIPTION:
LAST-MODIFIED:20200103T134558Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T110000Z
DTEND:20200317T123000Z
DTSTAMP:20200204T230237Z
UID:67h5d42vdri8o3b1ge7v8acirp@google.com
CREATED:20191227T122839Z
DESCRIPTION:
LAST-MODIFIED:20200103T134550Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T163000Z
DTEND:20200316T180000Z
DTSTAMP:20200204T230237Z
UID:013ktmqsnpluh59lg5fujh8m6k@google.com
CREATED:20191227T122833Z
DESCRIPTION:
LAST-MODIFIED:20200103T134546Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T110000Z
DTEND:20200316T123000Z
DTSTAMP:20200204T230237Z
UID:6kfbbi35gdiu8gh89p2rii823g@google.com
CREATED:20191227T122815Z
DESCRIPTION:
LAST-MODIFIED:20200103T134530Z
LOCATION:F1
SEQUENCE:2
STATUS:CONFIRMED
SUMMARY:FC02: Your Wit Is My Command (Veale)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T090000Z
DTEND:20200317T103000Z
DTSTAMP:20200204T230237Z
UID:6go79ua50hp6dbbaii17likfs5@google.com
CREATED:20191227T120317Z
DESCRIPTION:
LAST-MODIFIED:20200103T134504Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T090000Z
DTEND:20200316T103000Z
DTSTAMP:20200204T230237Z
UID:0pit14h5csunjclb6qtuui2klf@google.com
CREATED:20191227T120308Z
DESCRIPTION:
LAST-MODIFIED:20200103T134501Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T143000Z
DTEND:20200316T160000Z
DTSTAMP:20200204T230237Z
UID:23bku6miqqvhskg32qqcsvos9v@google.com
CREATED:20191227T120301Z
DESCRIPTION:
LAST-MODIFIED:20200103T134459Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:PC4: Curious Making\, Taking Fabrication Risks and Crafting Rewards
  (Meissner)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T163000Z
DTEND:20200314T180000Z
DTSTAMP:20200204T230237Z
UID:0qrqs5hjqdon53j0763sotb8ae@google.com
CREATED:20191227T114558Z
DESCRIPTION:
LAST-MODIFIED:20200103T131635Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T163000Z
DTEND:20200313T180000Z
DTSTAMP:20200204T230237Z
UID:5nfpq02g4qqgnopdkip91r3ts7@google.com
CREATED:20191227T114544Z
DESCRIPTION:
LAST-MODIFIED:20200103T131630Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T110000Z
DTEND:20200314T123000Z
DTSTAMP:20200204T230237Z
UID:4vcsce5ds83kbou6hp4lgqkqrm@google.com
CREATED:20191227T114129Z
DESCRIPTION:
LAST-MODIFIED:20200103T131625Z
LOCATION:F2
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T110000Z
DTEND:20200315T123000Z
DTSTAMP:20200204T230237Z
UID:13oubqg4tshsbu2l5ci79kal4t@google.com
CREATED:20191227T114514Z
DESCRIPTION:
LAST-MODIFIED:20200103T131623Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:IC4: Introduction to Philosophy (Felzmann)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T090000Z
DTEND:20200315T103000Z
DTSTAMP:20200204T230237Z
UID:7hu8ek5v6sl7g6b690r6m5qmqb@google.com
CREATED:20191227T121559Z
DESCRIPTION:
LAST-MODIFIED:20200103T131555Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T143000Z
DTEND:20200313T160000Z
DTSTAMP:20200204T230237Z
UID:3502m9d38fsnuicbo3hfj5rta3@google.com
CREATED:20191227T121428Z
DESCRIPTION:
LAST-MODIFIED:20200103T131553Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T090000Z
DTEND:20200314T103000Z
DTSTAMP:20200204T230237Z
UID:7ioi0uvbamaalc92uqp15d0n6m@google.com
CREATED:20191227T121552Z
DESCRIPTION:
LAST-MODIFIED:20200103T131551Z
LOCATION:F2
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:FC13: Human Computation (Krause &amp; Simperl)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T163000Z
DTEND:20200315T180000Z
DTSTAMP:20200204T230237Z
UID:1cbgmii2h3q0qbgs3siosbhsbj@google.com
CREATED:20191227T095641Z
DESCRIPTION:
LAST-MODIFIED:20191227T150729Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Pan1: Academia &amp; Industry Panel (Inkster)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200316T200000Z
DTEND:20200316T213000Z
DTSTAMP:20200204T230237Z
UID:5fes2q1runtvkaslmkpdd16qnv@google.com
CREATED:20191227T105028Z
DESCRIPTION:
LAST-MODIFIED:20191227T150608Z
LOCATION:Dining Hall
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET2: Data-Driven Dynamical Models for Neuroscience and Neuroenginee
 ring (Brunton)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200314T200000Z
DTEND:20200314T213000Z
DTSTAMP:20200204T230237Z
UID:3om15rdo11mkug8eei02lrfue1@google.com
CREATED:20191227T103753Z
DESCRIPTION:
LAST-MODIFIED:20191227T150604Z
LOCATION:Dining Hall
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET1: How to know (Kidd)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:044g33t1vjjo8fkhur8dme2g7e@google.com
CREATED:20191227T124200Z
DESCRIPTION:
LAST-MODIFIED:20191227T124200Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:29k4brksqb3n3brnus7g4a66e1@google.com
CREATED:20191227T124151Z
DESCRIPTION:
LAST-MODIFIED:20191227T124151Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:3o464kv33vvct2qkpafd0r69i4@google.com
CREATED:20191227T124143Z
DESCRIPTION:
LAST-MODIFIED:20191227T124143Z
LOCATION:H3
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PC1: Exploration\, curiosity and Not-Knowing stance – Perceiving th
 e World through Introspection (Vetter &amp; Reul)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:7gjm71e76ojbas79t3d101pihe@google.com
CREATED:20191227T123949Z
DESCRIPTION:
LAST-MODIFIED:20191227T123949Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:2nvddratom34sd4ar82k1ak3ga@google.com
CREATED:20191227T123941Z
DESCRIPTION:
LAST-MODIFIED:20191227T123941Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:4mhmqsfn3c57ds8g04fisasrn9@google.com
CREATED:20191227T123933Z
DESCRIPTION:
LAST-MODIFIED:20191227T123934Z
LOCATION:H4
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:FC05: Confidence and Overconfidence (Nityananda)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T090000Z
DTEND:20200319T103000Z
DTSTAMP:20200204T230237Z
UID:5v1armurdvtct1jv8f1dov6tt9@google.com
CREATED:20191227T115447Z
DESCRIPTION:
LAST-MODIFIED:20191227T115448Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200319T143000Z
DTEND:20200319T160000Z
DTSTAMP:20200204T230237Z
UID:5fleo3gadfif8229jbj7ki1hse@google.com
CREATED:20191227T115439Z
DESCRIPTION:
LAST-MODIFIED:20191227T115439Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T143000Z
DTEND:20200318T160000Z
DTSTAMP:20200204T230237Z
UID:7msrftqcil57e4k74f9hrkked5@google.com
CREATED:20191227T115432Z
DESCRIPTION:
LAST-MODIFIED:20191227T115432Z
LOCATION:F1
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T143000Z
DTEND:20200317T160000Z
DTSTAMP:20200204T230237Z
UID:252liug2nf9g18lq7m1jeac40c@google.com
CREATED:20191227T115422Z
DESCRIPTION:
LAST-MODIFIED:20191227T115426Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:MC2: Symbolic Reasoning within Connectionist Systems (Greff)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200318T200000Z
DTEND:20200318T213000Z
DTSTAMP:20200204T230237Z
UID:2oterd7css1ie2ahgccpooi1j8@google.com
CREATED:20191227T114846Z
DESCRIPTION:
LAST-MODIFIED:20191227T114846Z
LOCATION:Dining Hall
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:ET4: Cognitive Systems (Schultz)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T193000Z
DTEND:20200317T220000Z
DTSTAMP:20200204T230237Z
UID:51gneemai7pgnulg7oeefnqtkn@google.com
CREATED:20191227T114756Z
DESCRIPTION:
LAST-MODIFIED:20191227T114756Z
LOCATION:Dining Hall
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Conference Dinner
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200317T180000Z
DTEND:20200317T183000Z
DTSTAMP:20200204T230237Z
UID:7emeb2636l7pmu3irshvvtjm0p@google.com
CREATED:20191227T114654Z
DESCRIPTION:
LAST-MODIFIED:20191227T114734Z
LOCATION:F1
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:ET3: Information as a Resource: How Organisms Deal with Uncertainty
  (Kacelnik)
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200315T200000Z
DTEND:20200315T223000Z
DTSTAMP:20200204T230237Z
UID:2v0d82t4j6t28gpusp0qbjjpk7@google.com
CREATED:20191227T103914Z
DESCRIPTION:
LAST-MODIFIED:20191227T105106Z
LOCATION:Forum
SEQUENCE:3
STATUS:CONFIRMED
SUMMARY:Poster Session
TRANSP:OPAQUE
END:VEVENT
BEGIN:VEVENT
DTSTART:20200313T200000Z
DTEND:20200313T213000Z
DTSTAMP:20200204T230237Z
UID:4veloq7gfqlbn71p9oec2uq5dd@google.com
CREATED:20191227T103653Z
DESCRIPTION:
LAST-MODIFIED:20191227T103828Z
LOCATION:
SEQUENCE:1
STATUS:CONFIRMED
SUMMARY:Welcome Event
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR`;
