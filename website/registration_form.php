<?php
/* Template Name: Registration Form */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!-- Standard Wordpress 2017 theme page template -->
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/page/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			<!-- End of theme page template -->

			<!-- Utility functions for form submission via e-mail -->
			<!-- Created by: Benjamin Paaßen, October 2019 -->
			<?php
			/*
			 * If you wish to change any of the variables, you will have to
			 * check five locations in this file: first, the validation stage,
			 * starting around line 80; then, the construction of the
			 * registration mail (about line 330); the object
			 * representation of the registration data (about line 427);
			 * the validation of the stipend application data (about line 465);
			 * the construction of the stipend application mail
			 * (about line 546); and, finally, the object representation of the
			 * stipend application (about line 605).
			 */
				// definition of conference fee constants
				$conference_fees = [
					"EarlyBirdMemberFull" => 500,
					"EarlyBirdMemberPhd" => 240,
					"EarlyBirdMemberStudent" => 100,
					"EarlyBirdNonMemberFull" => 800,
					"EarlyBirdNonMemberPhd" => 320,
					"EarlyBirdNonMemberStudent" => 140,
					"RegularMemberFull" => 600,
					"RegularMemberPhd" => 300,
					"RegularMemberStudent" => 120,
					"RegularNonMemberFull" => 1000,
					"RegularNonMemberPhd" => 400,
					"RegularNonMemberStudent" => 170,
				];
				// definition of accommodation fee constants
				$accommodation_fees = [
					"External" => 300,
					"Single" => 510,
					"Double" => 430,
					"Triple" => 310,
				];
				// check whether this registration is early bird.
				// IMPORTANT: We assume here that the early bird deadline is
				// the 7th of January! Otherwise, this check has to be
				// changed!
				date_default_timezone_set('Europe/Berlin');
				$registration_date = date("m-d");
				if($registration_date >= "06-01" || $registration_date <= "01-07") {
					$conference_fee_string = 'EarlyBird';
				} else {
					$conference_fee_string = 'Regular';
				}

				// test whether we received a post request; otherwise, ignore it
				if ($_SERVER['REQUEST_METHOD'] != 'POST') {
					exit;
				}
				// validate and sanitize form data

				// if the second email field was used, the registration is treated
				// as spam
				if(isset($_POST['email2']) & $_POST['email2'] !== '') {
					echo 'You used the invisible second e-mail field which indicates that you are an automated spam-bot. If not, we apologize. Please fill out the form again (without this field) and re-submit. If this does not help, please contact the webmaster.';
					exit;
				}

				// salutation
				if(!isset($_POST['salute'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation was not specified. Please check your form again and re-submit.';
					exit;
				}
				$salute = sanitize_text_field( $_POST['salute'] );
				if(!($salute === 'Mr' || $salute === 'Ms' || $salute === 'Mx')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the salutation had an invalid value (must be either Ms, Mr, or Mx). Please check your form again and re-submit.';
					exit;
				}

				// title
				$title = "";
				if(isset($_POST['title_prof'])) {
					$title .= "Prof.";
					if(isset($_POST['title_dr'])) {
						$title .= " Dr.";
					}
				} else if(isset($_POST['title_dr'])) {
					$title .= "Dr.";
				}

				// first name
				if(!isset($_POST['firstname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the first name was not given. Please check your form again and re-submit.';
					exit;
				}
				$first_name = sanitize_text_field( $_POST['firstname'] );

				// last name
				if(!isset($_POST['lastname'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the last name was not given. Please check your form again and re-submit.';
					exit;
				}
				$last_name = sanitize_text_field( $_POST['lastname'] );

				// phone number
				if(!isset($_POST['phone'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the phone number was not given. Please check your form again and re-submit.';
					exit;
				}
				$phone = sanitize_text_field( $_POST['phone'] );

				// email
				if(!isset($_POST['email'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the e-mail address was not given. Please check your form again and re-submit.';
					exit;
				}
				$email = sanitize_text_field( $_POST['email'] );

				// street
				if(!isset($_POST['street'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$street = sanitize_text_field( $_POST['street'] );

				// house no.
				if(!isset($_POST['no'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$no = sanitize_text_field( $_POST['no'] );

				// zip
				if(!isset($_POST['zip'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$zip = sanitize_text_field( $_POST['zip'] );

				// city
				if(!isset($_POST['city'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$city = sanitize_text_field( $_POST['city'] );

				// country
				if(!isset($_POST['country'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your invoice address was not given. Please check your form again and re-submit.';
					exit;
				}
				$country = sanitize_text_field( $_POST['country'] );

				// faculty
				if(!isset($_POST['institution'])) {
					$institution = "";
				} else {
					$institution = sanitize_text_field( $_POST['institution'] );
				}

				// department
				if(!isset($_POST['department'])) {
					$department = "";
				} else {
					$department = sanitize_text_field( $_POST['department'] );
				}

				// field of study
				if(!isset($_POST['studyfield'])) {
					$studyfield = "";
				} else {
					$studyfield = sanitize_text_field( $_POST['studyfield'] );
				}

				// process billing address if checked
				if(isset($_POST['different_billing_address'])) {

					// faculty
					if(!isset($_POST['billing_institution'])) {
						$billing_institution = "";
					} else {
						$billing_institution = sanitize_text_field( $_POST['billing_institution'] );
					}

					// department
					if(!isset($_POST['billing_department'])) {
						$billing_department = "";
					} else {
						$billing_department = sanitize_text_field( $_POST['billing_department'] );
					}

					// street
					if(!isset($_POST['billing_street'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the street of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_street = sanitize_text_field( $_POST['billing_street'] );

					// house no.
					if(!isset($_POST['billing_no'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the house number of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_no = sanitize_text_field( $_POST['billing_no'] );

					// zip
					if(!isset($_POST['billing_zip'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the zip code of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_zip = sanitize_text_field( $_POST['billing_zip'] );

					// city
					if(!isset($_POST['billing_city'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the city of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_city = sanitize_text_field( $_POST['billing_city'] );

					// country
					if(!isset($_POST['billing_country'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the country of your billing address was not given. Please check your form again and re-submit.';
						exit;
					}
					$billing_country = sanitize_text_field( $_POST['billing_country'] );
				} else {
					// otherwise copy the data from the regular address
					$billing_institution = $institution;
					$billing_department = $department;
					$billing_street = $street;
					$billing_no = $no;
					$billing_city = $city;
					$billing_zip = $zip;
					$billing_country = $country;
				}

				// conference_fee
				if(!isset($_POST['conference_fee'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, your conference fee was not given. Please check your form again and re-submit.';
					exit;
				}
				$fee_class = sanitize_text_field( $_POST['conference_fee'] );
				if(!($fee_class === 'MemberStudent' || $fee_class === 'MemberPhd' || $fee_class === 'MemberFull' ||
					 $fee_class === 'NonMemberStudent' || $fee_class === 'NonMemberPhd' || $fee_class === 'NonMemberFull')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the conference fee had an invalid value (must be MemberStudent, MemberPhd, MemberFull, NonMemberStudent, NonMemberPhd, or NonMemberFull). Please check your form again and re-submit.';
					exit;
				}
				$conference_fee_string .= $fee_class;

				// GI membership number
				if(!isset($_POST['gi_number'])) {
					$gi_number = "";
				} else {
					$gi_number = sanitize_text_field( $_POST['gi_number'] );
				}

				// accommodation
				if(!isset($_POST['room'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the accommodation was not given. Please check your form again and re-submit.';
					exit;
				}
				$room = sanitize_text_field( $_POST['room'] );
				if(!($room === 'External' || $room === 'Single' || $room === 'Double' || $room === 'Triple')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the accommodation had an invalid value (must be External, Single, Double, or Triple). Please check your form again and re-submit.';
					exit;
				}
				if(($room === 'Double' || $room === 'Triple')) {
					if(!isset($_POST['roommates'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you specified that you wish to book a Double or Triple room but did not specify roommates. Please check your form again and re-submit.';
						exit;
					}
					$roommates = sanitize_text_field( $_POST['roommates'] );
				} else {
					$roommates = '';
				}

				// payment type
				if(!isset($_POST['payment'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type was not given. Please check your form again and re-submit.';
					exit;
				}
				$payment = sanitize_text_field( $_POST['payment'] );
				if(!($payment === 'Bank transfer' || $payment === 'Credit card')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the payment type had an invalid value (must be either Bank transfer or Credit card). Please check your form again and re-submit.';
					exit;
				}

				// dietary restrictions
				if(!isset($_POST['diet'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the dietary restrictions were not given. Please check your form again and re-submit.';
					exit;
				}
				$diet = sanitize_text_field( $_POST['diet'] );
				if(!($diet === 'Vegetarian' || $diet === 'Vegan' || $diet === 'Other' || $diet === 'None')) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the dietary restrictions had an invalid value (must be either Vegetarian, Vegan, Other, or None). Please check your form again and re-submit.';
					exit;
				}
				if($diet === 'Other') {
					if(!isset($_POST['diet_other_text'])) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, you specified Other dietary restrictions but not which one. Please check your form again and re-submit.';
						exit;
					}
					$diet_other_text = sanitize_text_field( $_POST['diet_other_text'] );
				} else {
					$diet_other_text = '';
				}

				// course preferences
				if(!isset($_POST['courses'])) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the course preferences were not given. Please check your form again and re-submit.';
					exit;
				}
				$courses = sanitize_text_field( $_POST['courses'] );

				// construct registration e-mail from user data
				$registration_mail  = "Salutation:         $salute\n";
				if($title !== '') {
					$registration_mail .= "Title:              $title\n";
				}
				$registration_mail .= "Last name:          $last_name\n";
				$registration_mail .= "First name:         $first_name\n\n";

				// print address
				if(!isset($_POST['different_billing_address'])) {
					// print the address table without second column
					$registration_mail .= "University/Company: $institution\n";
					$registration_mail .= "Faculty/Department: $department\n";
					$registration_mail .= "Street/PO Box:      $street $no\n";
					$registration_mail .= "ZIP:                $zip\n";
					$registration_mail .= "City:               $city\n";
					$registration_mail .= "Country:            $country\n";
					$registration_mail .= "Phone:              $phone\n";
					$registration_mail .= "E-Mail:             $email\n";
				} else {
					// print the personal and billing address side by side.
					// to achieve proper alignment of both columns, we need to
					// fill the first column with white spaces.

					// retrieve the maximum length of a field in the
					// personal address to achieve proper alignment
					$max_len = max(strlen($institution), strlen('Personal'), strlen($street . ' ' . $no), strlen($zip), strlen($city), strlen($country), strlen($department));
					// print the header for the address table
					$fill = str_repeat(' ', $max_len - strlen('Personal'));
					$registration_mail .= "                    Personal$fill Billing\n";

					$fill = str_repeat(' ', $max_len - strlen($institution));
					$registration_mail .= "University/Company: $institution$fill $billing_institution\n";
					$fill = str_repeat(' ', $max_len - strlen($department));
					$registration_mail .= "Faculty/Department: $department$fill $billing_department\n";
					$fill = str_repeat(' ', $max_len - strlen($street . ' ' . $no));
					$registration_mail .= "Street/PO Box       $street $no$fill $billing_street $billing_no\n";
					$fill = str_repeat(' ', $max_len - strlen($zip));
					$registration_mail .= "ZIP:                $zip$fill $billing_zip\n";
					$fill = str_repeat(' ', $max_len - strlen($city));
					$registration_mail .= "City:               $city$fill $billing_city\n";
					$fill = str_repeat(' ', $max_len - strlen($country));
					$registration_mail .= "Country:            $country$fill $billing_country\n";
					$registration_mail .= "Phone:              $phone\n";
					$registration_mail .= "E-Mail:             $email\n";
				}
				$registration_mail .= "Field of Study:     $studyfield\n";
				$registration_mail .= "Share room with:    $roommates\n";
				$registration_mail .= "Food information:   $diet";
				if($diet === 'Other') {
					$registration_mail .= " ($diet_other_text)";
				}
				if($gi_number === '') {
					$registration_mail .= "\n";
				} else {
					$registration_mail .= "\nGI number:          $gi_number\n";
				}
				$registration_mail .= "Courses:            $courses\n\n";
				// print the payment information. Again, we format the
				// information in a two-column layout, such that we need to
				// fill the first column with white spaces to get the
				// alignment right
				$max_len = max(strlen($conference_fee_string), strlen($room));

				$registration_mail .= "Payment:            $payment\n";

				$conference_fee     = $conference_fees[$conference_fee_string];
				$fill = str_repeat(' ', $max_len - strlen($conference_fee_string));
				$registration_mail .= "Conference fee:     $conference_fee_string$fill $conference_fee EUR\n";

				$fill = str_repeat(' ', $max_len - strlen($room));
				$accommodation_fee  = $accommodation_fees[$room];
				$registration_mail .= "Accommodation fee:  $room$fill $accommodation_fee EUR\n";

				$fill = str_repeat(' ', $max_len);
				$total_fee = $conference_fee + $accommodation_fee;
				$registration_mail .= "Total amount:       $fill $total_fee EUR\n\n";

				if(isset($_POST['stipend_application'])) {
					$registration_mail .= "This participant has also applied for a stipend.";
				}

				// check that the data has not become too long overall, which
				// would be an indication of some kind of hacking attack
				if(strlen($registration_mail) > 5000) {
					echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the registration data was too long (> 5000 characters). Please check your form again and re-submit.';
					exit;
				}

				// create a machine readable version in JSON format
				$registration_object = (object)[];
				$registration_object->salutation = $salute;
				$registration_object->title = $title;
				$registration_object->first_name = $first_name;
				$registration_object->last_name = $last_name;
				$registration_object->phone = $phone;
				$registration_object->email = $email;
				$registration_object->address = (object)[];
				$registration_object->address->institution = $institution;
				$registration_object->address->department = $department;
				$registration_object->address->street = $street;
				$registration_object->address->no = $no;
				$registration_object->address->zip = $zip;
				$registration_object->address->city = $city;
				$registration_object->address->country = $country;
				$registration_object->studyfield = $studyfield;
				$registration_object->billing_address = (object)[];
				$registration_object->billing_address->billing_institution = $billing_institution;
				$registration_object->billing_address->billing_department = $billing_department;
				$registration_object->billing_address->billing_street = $billing_street;
				$registration_object->billing_address->billing_no = $billing_no;
				$registration_object->billing_address->billing_zip = $billing_zip;
				$registration_object->billing_address->billing_city = $billing_city;
				$registration_object->billing_address->billing_country = $billing_country;
				$registration_object->conference_fee_string = $conference_fee_string;
				$registration_object->gi_number = $gi_number;
				$registration_object->room = $room;
				$registration_object->roommates = $roommates;
				$registration_object->payment = $payment;
				$registration_object->conference_fee = $conference_fee;
				$registration_object->accommodation_fee = $accommodation_fee;
				$registration_object->diet = $diet;
				$registration_object->diet_other_text = $diet_other_text;
				$registration_object->courses = $courses;
				$registration_object->registration_date = date("Y-m-d H:i:s");

				// now, check stipend application data
				if(isset($_POST['stipend_application'])) {

					// organisations
					if(isset($_POST['stipend_organisations'])) {
						$stipend_organisations = sanitize_text_field( $_POST['stipend_organisations'] );
					} else {
						$stipend_organisations = '';
					}

					// poster planned
					if(isset($_POST['poster'])) {
						$poster = 'true';
					} else {
						$poster = 'false';
					}

					// rainbow talk planned
					if(isset($_POST['rainbow'])) {
						$rainbow = 'true';
					} else {
						$rainbow = 'false';
					}

					// sunshine lecturers
					if(isset($_POST['sunshine_lecturers'])) {
						$sunshine_lecturers = sanitize_text_field( $_POST['sunshine_lecturers'] );
					} else {
						$sunshine_lecturers = '';
					}

					// motivation letter
					if(!(isset($_POST['motivation']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the motivation letter was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$motivation = sanitize_text_field( $_POST['motivation'] );

					if(strlen($motivation) > 1600) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the motivation letter in the stipend application form was too long (> 1500 characters). Please check your form again and re-submit.';
						exit;
					}

					if(!(isset($_POST['additional_funding']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the additional funding was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$additional_funding = sanitize_text_field( $_POST['additional_funding'] );

					if(!(isset($_POST['monthly_income']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the monthly income was not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$monthly_income = sanitize_text_field( $_POST['monthly_income'] );

					if(!(isset($_POST['monthly_expenses']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the monthly expenses were not given in the stipend application form. Please check your form again and re-submit.';
						exit;
					}
					$monthly_expenses = sanitize_text_field( $_POST['monthly_expenses'] );

					// stipend requirement
					if(!(isset($_POST['stipend_requirement']))) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, it was not specified how urgent you need a stipend. Please check your form again and re-submit.';
						exit;
					}
					$stipend_requirement = sanitize_text_field( $_POST['stipend_requirement'] );
					if(!($stipend_requirement === 'Full stipend required' || $stipend_requirement === 'Partial stipend required' || $stipend_requirement === 'Stipend helpful')) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the stipend urgency had an invalid value (must be either Full stipend required, Partial stipend required, or Stipend helpful). Please check your form again and re-submit.';
						exit;
					}

					// retrieve student status from conference fee string
					if(substr($conference_fee_string, -strlen('Student')) === 'Student') {
						$status = 'Student';
					} else if(substr($conference_fee_string, -strlen('Phd')) === 'Phd') {
						$status = 'PhD Student';
					} else {
						$status = 'Other';
					}

					// construct a stipend application e-mail from user data
					$stipend_mail  = "Salutation:     $salute\n";
					$stipend_mail .= "First Name:     $first_name\n";
					$stipend_mail .= "Last Name:      $last_name\n";
					$stipend_mail .= "E-Mail:         $email\n";
					$stipend_mail .= "Student Status: $status\n";
					$stipend_mail .= "Field of Study: $studyfield\n";
					$stipend_mail .= "Institution:    $institution, $department\n";
					$stipend_mail .= "Organisations:  $stipend_organisations\n\n";

					$stipend_mail .= "Planned contributions: ";
					if($poster === 'true') {
						$stipend_mail .= 'Poster, ';
					}
					if($rainbow === 'true') {
						$stipend_mail .= 'Rainbow talk, ';
					}
					if(!($sunshine_lecturers === '')) {
						$stipend_mail .= "Sunshine for $sunshine_lecturers";
					}
					$stipend_mail .= "\n\nMotivation letter:\n\n";
					$stipend_mail .= wordwrap($motivation);
					$stipend_mail .= "\n\n";
					$stipend_mail .= "Conference Fee:     $conference_fee_string $conference_fee EUR\n";
					$stipend_mail .= "Accommodation Fee:  $room $accommodation_fee EUR\n";
					$stipend_mail .= "Monthly income:     $monthly_income EUR\n";
					$stipend_mail .= "Monthly expenses:   $monthly_expenses EUR\n";
					$stipend_mail .= "Additional Funding: $additional_funding EUR\n";
					$stipend_mail .= "Urgency:            $stipend_requirement\n\n";
					$stipend_mail .= "CSV row version of the data:\n\n";

					// write the data again, but this time in a single row for
					// a CSV table of all applicants
					// write the column headers for a CSV row representation
					$stipend_mail .= "\"Salutation\";\"First name\";\"Last name\";\"E-mail\";";
					$stipend_mail .= "\"Student Status\";\"Field of Study\";";
					$stipend_mail .= "\"Institution\";\"Department\";";
					$stipend_mail .= "\"Organisations\";";
					$stipend_mail .= "\"Poster planned\";\"Rainbow Talk planned\";\"Sunshine for\";";
					$stipend_mail .= "\"Motivation Letter\";";
					$stipend_mail .= "\"Conference Fee\";\"Accommodation Fee\";\"Total Fee\";";
					$stipend_mail .= "\"Monthly Income\";\"Monthly Expenses\";\"Additional Funding\";\"Urgency\"\n";
					// write the actual data
					$stipend_mail .= "\"$salute\";\"$first_name\";\"$last_name\";\"$email\";";
					$stipend_mail .= "\"$status\";\"$studyfield\";";
					$stipend_mail .= "\"$institution\";\"$department\";";
					$stipend_mail .= "\"$stipend_organisations\";";
					$stipend_mail .= "\"$poster\";\"$rainbow\";\"$sunshine_lecturers\";";
					$stipend_mail .= "\"" . preg_replace('/\s\s+/', ' ', $motivation) . "\";";
					$stipend_mail .= "\"$conference_fee\";\"$accommodation_fee\";\"$total_fee\";";
					$stipend_mail .= "\"$monthly_income\";\"$monthly_expenses\";\"$additional_funding\";\"$stipend_requirement\"";

					// check that the data has not become too long overall, which
					// would be an indication of some kind of hacking attack
					if(strlen($stipend_mail) > 5000) {
						echo 'Unfortunately, your form data was invalid, even though we checked it in javascript. In particular, the stipend application data was too long (> 5000 characters). Please check your form again and re-submit.';
						exit;
					}

					// append the object representation of the stipend application data to the
					// registration object
					$registration_object->stipend_application = (object)[];
					$registration_object->stipend_application->stipend_organisations = $stipend_organisations;
					$registration_object->stipend_application->poster = $poster;
					$registration_object->stipend_application->rainbow = $rainbow;
					$registration_object->stipend_application->sunshine_lecturers = $sunshine_lecturers;
					$registration_object->stipend_application->motivation = $motivation;
					$registration_object->stipend_application->additional_funding = $additional_funding;
					$registration_object->stipend_application->monthly_income = $monthly_income;
					$registration_object->stipend_application->monthly_expenses = $monthly_expenses;
					$registration_object->stipend_application->stipend_requirement = $stipend_requirement;

				}

				// special code for debug input
				if($first_name === 'Debug') {
					echo "<p>Your registration was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice via letter. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
					echo "<p>Your stipend application was sent successfully to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You should receive the decision regarding the stipends via e-mail within the following weeks. For your own archive: The following data was transmitted.</p><pre>$stipend_mail</pre>";
					$json_data = json_encode($registration_object);
					echo "<p>JSON data</p><pre>$json_data</pre>";
					exit;
				}

				$headers = array('Content-Type: text/html; charset=UTF-8');

				/*
				 * At this point, we are reasonably certain that the input is valid and that we
				 * can risk sending it via e-mail. Sent the registration data
				 */
				if(!wp_mail('registration@interdisciplinary-college.org', 'IK Waiting List Entry', '<pre>' . $registration_mail . '</pre>', $headers)) {
					// if the mail sending has failed, inform the user
					echo "<p>We are very sorry, but unfortunately sending your registration has failed. Please send your registration manually to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$registration_mail</pre>";
					// Also display the stipend application data, in case a stipend application
					// was handed in
					if(isset($_POST['stipend_application'])) {
						echo "<p>Additionally, please send your stipend application to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$stipend_mail</pre>";
					}
					exit;
				} else {
					// otherwise, display a success message
					echo "<p>Your waiting list application was sent successfully to <a href=\"mailto:registration@interdisciplinary-college.org\">registration@interdisciplinary-college.org</a>. Please note that your registration is only confirmed once you received your invoice via letter. For your own archive: The following data was transmitted:</p> <pre>$registration_mail</pre>";
				}

				// send the stipend application if there was one
				if(isset($_POST['stipend_application'])) {
					if(!wp_mail('stipends@interdisciplinary-college.org', 'IK Stipend Application', '<pre>' . $stipend_mail . '</pre>', $headers)) {
						// if sending the stipend application failed, inform the user.
						echo "<p>We are very sorry, but unfortunately sending your stipend application has failed. Please send your stipend application manually to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You can just copy & paste the following text into the e-Mail:</p><pre>$stipend_mail</pre>";
						exit;
					} else {
						// otherwise, display a success message
						echo "<p>Your stipend application was sent successfully to <a href=\"mailto:stipends@interdisciplinary-college.org\">stipends@interdisciplinary-college.org</a>. You should receive the decision regarding the stipends via e-mail within the following weeks. For your own archive: The following data was transmitted.</p><pre>$stipend_mail</pre>";
					}
				}

				$headers = array('Content-Type: text/plain; charset=UTF-8');

				// send a machine-readable backup copy to the webmaster
				wp_mail('webmaster@interdisciplinary-college.org', 'IK Waiting List Entry (machine readable copy)', json_encode($registration_object), $headers)

			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer(); ?>
