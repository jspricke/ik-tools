#!/usr/bin/python3
"""
This script processes an input text file with one mail addres per line
and produces an output file with alphabetically sorted emails without
duplicates

Copyright (C) 2019
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2019 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import sys

if len(sys.argv) < 2:
	raise ValueError('Expected a file name as input argument!')

input_file = sys.argv[1]

if len(sys.argv) >= 3:
	output_file = sys.argv[2]
else:
	output_file = 'unique_mails.txt'

# create a set with all e-mail addresses
unique_mails = set()

# iterate over every line in the input file and add the line to the set
with open(input_file) as f:
	for email in f:
		unique_mails.add(email)

# write out
with open(output_file, 'w') as f:
	for email in sorted(unique_mails):
		f.write(email)
